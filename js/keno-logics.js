//https://www.w3schools.com/js/js_json_arrays.asp
//https://www.w3schools.com/js/tryit.asp?filename=tryjson_array_loop_in
//https://www.w3schools.com/js/js_json_objects.asp

// Fyah Creator DB




var prevBtn;
var nextBtn;
var eyeSet;
var bocaSet;
var dichoSet;
var headSet;
var radios;

window.onload = function () {
     prevBtn = document.getElementById('prevBtn');
     nextBtn = document.getElementById('nextBtn');
     eyeSet = document.getElementById('eyeSet');
     bocaSet = document.getElementById('bocaSet');
     dichoSet = document.getElementById('dichoSet');
     headSet = document.getElementById('headSet');
     radios = document.getElementsByName('facePart');

    prevBtn.addEventListener("click", prevFace);
    nextBtn.addEventListener("click", nextFace);
    console.log("Loaded");
};

var imageUrl = "images/fyah";
var myEyes = {
    "position": 0,
    "urls": ["/eyes1.png", "/eyes2.png", "/eyes3.png", "/eyes4.png", "/eyes5.png", "/eyes6.png", "/eyes7.png","/eyes8Girl.png","/eyes9Ra.png","/eyes10Electric.png","/eyes11zippo.png","/eyes12antorcha.png"]
};

var myBocas = {
    "position": 0,
    "urls": ["/boca1.png", "/boca2.png", "/boca3.png", "/boca4.png", "/boca5.png", "/boca6.png", "/boca7.png","/boca8Felix.png","/boca9Ra.png","/boca10Electric.png","/boca11Zippo.png","/boca12Antorcha.png"]
};

var myBody = {
    "position": 0,
    "urls": ["/body1.jpg", "/body2.jpg", "/body3.jpg", "/body4.jpg", "/body5.jpg", "/body6.jpg", "/body7.jpg","/body8Phoenix.jpg","/body9Ra.jpg","/body10Electric.jpg","/body11Zippo.jpg","/body12Antorcha.jpg"]
};


/*Functions For Emoji Creator
vvvvvvvvvvvvvvvvvvvvvvvvvvv
*/

function changeFace() {
    console.log("Change Face Method");
    //works along with Radio Bottom
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            //alert ("about to change the "+ radios[i].value);
            if (radios[i].value === "eyes") { //eyes changes here:
                var nPositions = myEyes.urls.length - 1;

                do {
                    var xNumber = Math.floor((Math.random() * nPositions) + 0);
                }
                while (xNumber === myEyes.position);
                eyeSet.src = imageUrl + myEyes.urls[xNumber];
                myEyes.position = xNumber;

                break;
            } else if (radios[i].value === "boca") { //boca changes here:
                var nPositions = myBocas.urls.length - 1;

                do {
                    var xNumber = Math.floor((Math.random() * nPositions) + 0);
                }
                while (xNumber === myBocas.position);
                bocaSet.src = imageUrl + myBocas.urls[xNumber];
                myBocas.position = xNumber;

                break;
            } else if (radios[i].value === "cejas") { //cejas changes here:
                var nPositions = myBody.urls.length - 1;

                do {
                    var xNumber = Math.floor((Math.random() * nPositions) + 0);
                }
                while (xNumber === myBody.position);
                headSet.src = imageUrl + myBody.urls[xNumber];
                myBody.position = xNumber;

                break;
            }

        }
    }
}
function randomFace() {
    console.log("Random Face Method");
    var eyesPositions = myEyes.urls.length - 1;

    do {
        var xNumber = Math.floor((Math.random() * eyesPositions) + 0);
    }
    while (xNumber === myEyes.position);
    eyeSet.src = imageUrl + myEyes.urls[xNumber];
    myEyes.position = xNumber;
    //cejas changes here:
    var cejasPositions = myBody.urls.length - 1;

    do {
        var xNumber = Math.floor((Math.random() * cejasPositions) + 0);
    }
    while (xNumber === myBody.position);
    headSet.src = imageUrl + myBody.urls[xNumber];
    myBody.position = xNumber;


    //boca changes here:
    var bocaPositions = myBocas.urls.length - 1;

    do {
        var xNumber = Math.floor((Math.random() * bocaPositions) + 0);
    }
    while (xNumber === myBocas.position);
    bocaSet.src = imageUrl + myBocas.urls[xNumber];
    myBocas.position = xNumber;

}

function nextFace() {
    console.log("Next Face Method");
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            //alert ("about to change the "+ radios[i].value);
            if (radios[i].value === "eyes") { //eyes changes here:
                var movimiento = myEyes.position;
                var positionsArray = myEyes.urls.length - 1;
                //alert (movimiento +"-" +positionsArray);
                //alert("current position in json " + myEyes.position);
                //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
                if (movimiento <= positionsArray) {
                    var sum = movimiento + 1;
                    //alert("entro " + sum);
                    if (sum <= positionsArray) {
                        movimiento = sum;
                        eyeSet.src = imageUrl + myEyes.urls[movimiento];
                        myEyes.position = movimiento;
                    } else {
                        //alert("ultimo " + sum);
                        eyeSet.src = imageUrl + myEyes.urls[0];
                        myEyes.position = 0;
                    }
                }
                break;
            } else if (radios[i].value === "boca") { //boca changes here:
                var movimiento = myBocas.position;
                var positionsArray = myBocas.urls.length - 1;
                //alert (movimiento +"-" +positionsArray);
                //alert("current position in json " + myEyes.position);
                //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
                if (movimiento <= positionsArray) {
                    var sum = movimiento + 1;
                    //alert("entro " + sum);
                    if (sum <= positionsArray) {
                        movimiento = sum;
                        bocaSet.src = imageUrl + myBocas.urls[movimiento];
                        myBocas.position = movimiento;
                    } else {
                        //alert("ultimo " + sum);
                        bocaSet.src = imageUrl + myBocas.urls[0];
                        myBocas.position = 0;
                    }
                }
                break;
            } else if (radios[i].value === "bodies") { //cejas changes here:
                var movimiento = myBody.position;
                var positionsArray = myBody.urls.length - 1;
                if (movimiento <= positionsArray) {
                    var sum = movimiento + 1;
                    //alert("entro " + sum);
                    if (sum <= positionsArray) {
                        movimiento = sum;
                        headSet.src = imageUrl + myBody.urls[movimiento];
                        myBody.position = movimiento;
                    } else {
                        //alert("ultimo " + sum);
                        headSet.src = imageUrl + myBody.urls[0];
                        myBody.position = 0;
                    }
                }
                break;
            }

        }
    }

}
function prevFace() {
    console.log("Previous Face Method");
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            //alert ("about to change the "+ radios[i].value);
            if (radios[i].value === "eyes") { //eyes changes here:
                var movimiento = myEyes.position;
                //alert("current position in json " + myEyes.position);
                //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
                //alert("tamanho array "+ myEyes.urls.length); //muestra tamanho desde 1.. el array cuenta desde 0
                if (movimiento >= 0) {
                    //alert("moviemento "+ movimiento + "new position in json " + myEyes.position);
                    //alert("NEXT url "+ imageUrl + myEyes.urls[myEyes.position]);
                    //alert("movimiento " +movimiento);
                    var rest = movimiento - 1;
                    if (rest < 0) {
                        movimiento = myEyes.urls.length - 1;
                        eyeSet.src = imageUrl + myEyes.urls[movimiento];
                        myEyes.position = movimiento;
                    } else {
                        eyeSet.src = imageUrl + myEyes.urls[movimiento - 1];
                        myEyes.position = movimiento - 1;
                    }
                }
                break;
            } else if (radios[i].value === "boca") { //eyes changes here:
                var movimiento = myBocas.position;
                //alert("current position in json " + myEyes.position);
                //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
                //alert("tamanho array "+ myEyes.urls.length); //muestra tamanho desde 1.. el array cuenta desde 0
                if (movimiento >= 0) {
                    //alert("moviemento "+ movimiento + "new position in json " + myEyes.position);
                    //alert("NEXT url "+ imageUrl + myEyes.urls[myEyes.position]);
                    //alert("movimiento " +movimiento);
                    var rest = movimiento - 1;
                    if (rest < 0) {
                        movimiento = myBocas.urls.length - 1;
                        bocaSet.src = imageUrl + myBocas.urls[movimiento];
                        myBocas.position = movimiento;
                    } else {
                        bocaSet.src = imageUrl + myBocas.urls[movimiento - 1];
                        myBocas.position = movimiento - 1;
                    }
                }
                break;
            } else if (radios[i].value === "bodies") { //eyes changes here:
                var movimiento = myBody.position;
                //alert("current position in json " + myEyes.position);
                //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
                //alert("tamanho array "+ myEyes.urls.length); //muestra tamanho desde 1.. el array cuenta desde 0
                if (movimiento >= 0) {
                    //alert("moviemento "+ movimiento + "new position in json " + myEyes.position);
                    //alert("NEXT url "+ imageUrl + myEyes.urls[myEyes.position]);
                    //alert("movimiento " +movimiento);
                    var rest = movimiento - 1;
                    if (rest < 0) {
                        movimiento = myBody.urls.length - 1;
                        headSet.src = imageUrl + myBody.urls[movimiento];
                        myBody.position = movimiento;
                    } else {
                        headSet.src = imageUrl + myBody.urls[movimiento - 1];
                        myBody.position = movimiento - 1;
                    }
                }
                break;
            }

        }//radio check
    }

}



/* Back to Top
vvvvvvvvvvvvvvvvv*/
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () { scrollFunction() };

function scrollFunction() {
    if (document.body.scrollTop > 420 || document.documentElement.scrollTop > 420) {
        //console.log("H:" +document.body.scrollTop);
        document.getElementById("myBtnTop").style.display = "block";
    } else {
        document.getElementById("myBtnTop").style.display = "none";
    }
}

//https://youtu.be/ykrupgQgmkA?t=500

//by @Keno10cr .: